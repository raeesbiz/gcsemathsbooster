//
//  QuizResultsViewController.swift
//  MathQuiz
//
//  Created by Raees on 26/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import UIKit

class QuizResultsViewController: UIViewController {

    @IBOutlet weak var resultsDescription: UILabel!
    @IBOutlet weak var resultsVisual: QuizPieChart!
    public var quiz: Quiz?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let correctAnswers = quiz?.correctAnswers() {
            let percentage = "\(correctAnswers * 100)% correct answers"
            if let grade = quiz?.grade() {
                resultsDescription.text = QuizConstants.ResultsLabelText.replacingOccurrences(of: "RESULT_TEXT", with: percentage).replacingOccurrences(of: "G_T", with: grade)
            }
        } else {
            resultsDescription.text = QuizConstants.ResultsLabelText.replacingOccurrences(of: "RESULT_TEXT", with: "").replacingOccurrences(of: "G_T", with: "")
        }
        addSegments()
        quiz?.reset()
    }
    
    private func addSegments() {
        if let incorrectAnswers = quiz?.incorrectAnswers() {
            if let correctAnswers = quiz?.correctAnswers() {
                resultsVisual.segments = [
                    Segment(color: UIColor.init(red: CGFloat(0.5), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(1)), value: CGFloat(incorrectAnswers)),
                    Segment(color: UIColor.init(red: CGFloat(0), green: CGFloat(0.5), blue: CGFloat(0), alpha: CGFloat(1)), value: CGFloat(correctAnswers)),
                ]
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
