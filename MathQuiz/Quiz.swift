//
//  Quiz.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

public class Quiz {
    
    public var questions = [Question]()
    public var title = ""
    public var questionCount = 0
    var currentQuestionIndex = 0
    var correctAnswerCount = 0
    public var completed = false
    
    public func correctAnswers() -> Double {
        return Double(correctAnswerCount) / Double(questionCount)
    }
    
    public func incorrectAnswers() -> Double {
        return Double(questionCount - correctAnswerCount) / Double(questionCount)
    }
    
    public func setCount() {
        questionCount = questions.count
    }
    
    public func isOnFirstQuestion() -> Bool {
        return currentQuestionIndex == 0
    }
    
    public func addCorrectAnswer() {
        correctAnswerCount = correctAnswerCount + 1
    }
    
    public func removeCorrectAnswer() {
        correctAnswerCount = correctAnswerCount - 1
    }
    public func getCurrentQuestion() -> Question? {
        if currentQuestionIndex < questionCount {
            return questions[currentQuestionIndex]
        } else {
            return nil
        }
    }
    
    public func getNextQuestion() -> Question? {
        currentQuestionIndex = currentQuestionIndex + 1
        if currentQuestionIndex < questionCount {
            return questions[currentQuestionIndex]
        } else {
            currentQuestionIndex = 0
            completed = true
            return nil
        }
    }
    
    public func getPreviousQuestion() -> Question? {
        currentQuestionIndex = currentQuestionIndex - 1
        if currentQuestionIndex >= 0 && currentQuestionIndex < questionCount {
            return questions[currentQuestionIndex]
        } else {
            currentQuestionIndex = 0
            return nil
        }
    }
    public func reset() {
        completed = false
        currentQuestionIndex = 0
        correctAnswerCount = 0
    }
    
    public func grade() -> String? {
        let percentage = correctAnswers()*100
        for (grade, range) in QuizConstants.PercentageToGrade {
            if range.contains(percentage) {
                return grade
            }
        }
        return nil
    }
    
}
