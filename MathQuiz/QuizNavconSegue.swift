//
//  QuizNavconSegue.swift
//  MathQuiz
//
//  Created by Raees on 26/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import UIKit

class QuizNavconSegue: UIStoryboardSegue {

    override func perform() {
        if let navCon = source.navigationController as UINavigationController? {
            var controllers = navCon.viewControllers
            controllers.removeLast()
            controllers.append(destination)
            navCon.setViewControllers(controllers, animated: true)
        }
    }
}
