//
//  QuizViewController.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var answers: UITableView!
    var quiz: Quiz?
    var question: Question?
    @IBOutlet weak var questionDescription: UILabel!
    @IBOutlet weak var back: UIButton!
    
    @IBAction func onBack(_ sender: Any) {
        question = quiz?.getPreviousQuestion()
        if let selectedAnswer = question?.selectedAnswer {
            if (question?.isCorrectAnswer(question: selectedAnswer)) != nil {
                quiz?.removeCorrectAnswer()
            }
        }
        reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = quiz?.title
        questionDescription.text = question?.description
        
        if let isOnFirstQuestion = quiz?.isOnFirstQuestion() {
            if isOnFirstQuestion {
                back.isHidden = true
            } else {
                back.isHidden = false
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QuizConstants.AnswerTableViewCellIdentifier, for: indexPath)
        cell.textLabel?.text = question?.answers[indexPath.row]
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let answer = question?.answers[indexPath.row] {
            let alert = UIAlertController(title: QuizConstants.PopupTitle, message: QuizConstants.PopupMessage, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: QuizConstants.PopupAction1Title, style: .default, handler: { action in
                self.updateQuestion(answer: answer)
            }))
            
            alert.addAction(UIAlertAction(title: QuizConstants.PopupAction2Title, style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    private func updateQuestion(answer: String) {
        if let isCorrectAnswer = question?.isCorrectAnswer(question: answer) {
            question?.selectedAnswer = answer
            if isCorrectAnswer {
                quiz?.addCorrectAnswer()
            }
            question = quiz?.getNextQuestion()
            if let quizCompleted = quiz?.completed {
                if quizCompleted {
                    performSegue(withIdentifier: "results", sender: nil)
                } else {
                    reloadData()
                }
            }
        }
    }
    
    private func reloadData() {
        questionDescription.text = question?.description
        answers?.reloadData()
        if let isOnFirstQuestion = quiz?.isOnFirstQuestion() {
            if isOnFirstQuestion {
                back.isHidden = true
            } else {
                back.isHidden = false
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfQuestions = question?.answers.count {
            return numberOfQuestions
        }
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let resultsMVC = segue.destination as? QuizResultsViewController {
            resultsMVC.quiz = quiz
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
