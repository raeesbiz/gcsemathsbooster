//
//  ViewController.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import UIKit

class QuizHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var quizes: UITableView!
    
    var container = QuizContainer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QuizConstants.HomeTableViewCellIdentifier, for: indexPath)
        cell.textLabel?.text = container.quizAt(indexPath)?.title
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.quizes.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let quizMVC = segue.destination as? QuizViewController {
            if let indexPath = quizes?.indexPathForSelectedRow {
                let quiz = container.quizAt(indexPath)
                quizMVC.quiz = quiz
                quizMVC.question = quiz?.getCurrentQuestion()
            }
        }
    }
}
