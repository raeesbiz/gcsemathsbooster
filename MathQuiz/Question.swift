//
//  Question.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

public class Question {
    
    public var description = ""
    public var answers = [String]()
    public var correctAnswerIndex = 0
    public var selectedAnswer = ""
    public func isCorrectAnswer(question answer: String) -> Bool {
        let correctAnswer = answers[correctAnswerIndex]
        return answer == correctAnswer
    }
}
