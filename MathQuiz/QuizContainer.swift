//
//  QuizContainer.swift
//  MathQuiz
//
//  Created by Raees on 26/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

public class QuizContainer {
    
    var quizes = [Quiz]()
    
    init() {
        if let urls = Bundle.main.urls(forResourcesWithExtension: "xml", subdirectory: "quizes") {
            let parser = QuizXMLParser()
            for url in urls {
                parser.beginParsing(file: url)
                parser.quiz.setCount()
                quizes.append(parser.quiz)
                parser.reset()
            }
        }
    }
    
    public func quizAt(_ indexPath: IndexPath) -> Quiz? {
        return quizes[indexPath.row]
    }
}
