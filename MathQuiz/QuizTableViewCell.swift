//
//  QuizTableViewCell.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import UIKit

class QuizTableViewCell: UITableViewCell {

    var titleText: String?
    @IBOutlet weak var quizTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        quizTitle.text = titleText
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
