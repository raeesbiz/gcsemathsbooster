//
//  QuizXMLParser.swift
//  MathQuiz
//
//  Created by Raees on 22/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

public class QuizXMLParser : NSObject, XMLParserDelegate {
    
    var currentContent = ""
    var quiz = Quiz()
    var question = Question()
    
    public func beginParsing(file url: URL) {
        guard let parser = XMLParser(contentsOf: url) else {
            print("Cannot Read Data")
            return
        }
        parser.delegate = self
        if !parser.parse(){
            print("Data Errors Exist:")
            let error = parser.parserError!
            print("Error Description:\(error.localizedDescription)")
            print("Line number: \(parser.lineNumber)")
        }
    }
    
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]){
        if elementName == "question" {
            question = Question()
        }
        currentContent = ""
    }
    
    public func parser(_ parser: XMLParser, foundCharacters string: String){
        currentContent += string
    }
    
    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?){
        switch elementName{
        case "question":
            quiz.questions.append(question)
        case"title":
            quiz.title = currentContent
        case "correctAnswerIndex":
            if let index = Int(currentContent) {
                question.correctAnswerIndex = index
            }
        case "description":
            question.description = currentContent
        case "answer":
            question.answers.append(currentContent)
        default:
            return
        }
    }
    
    public func reset() {
        quiz = Quiz()
        question = Question()
    }
}
