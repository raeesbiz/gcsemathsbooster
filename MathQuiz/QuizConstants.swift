//
//  QuizConstants.swift
//  MathQuiz
//
//  Created by Raees on 26/06/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

struct QuizConstants {
    static let PopupTitle = "Your Answer"
    static let PopupMessage = "Do you want to proceed to the next question?"
    static let PopupAction1Title = "Yes"
    static let PopupAction2Title = "No"
    static let HomeTableViewCellIdentifier = "Quiz"
    static let AnswerTableViewCellIdentifier = "Answer"
    static let ResultsLabelText = "Congratulations on completing the quiz. \nScore: \nRESULT_TEXT \nGrade: G_T"
    static let PercentageToGrade = ["F":0.0..<40, "E":40.0..<50, "D":50.0..<60, "C":60.0..<70, "B":70.0..<80, "A":80.0..<100.1]
}
